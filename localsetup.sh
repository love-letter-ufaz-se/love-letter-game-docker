#!/bin/bash

git clone git@gitlab.com:love-letter-ufaz-se/love-letter-game-frontend.git
git clone git@gitlab.com:love-letter-ufaz-se/love-letter-game-users-app.git
git clone git@gitlab.com:ShapedHorizon/redis-set-list-expiration-manager.git
git clone git@gitlab.com:love-letter-ufaz-se/love-letter-game-game-app.git
git clone git@gitlab.com:love-letter-ufaz-se/love-letter-game-session-app.git
git clone git@gitlab.com:love-letter-ufaz-se/love-letter-game-api-app.git
