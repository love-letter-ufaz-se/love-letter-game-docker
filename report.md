# UFAZ SE Project - Love Letter Game

Team members:
- Aghateymur Hasanzada
- Kanan Mikayilov
- Gulzar Safarli
- Sadig Gojayev

## Table Of Contents

- [UFAZ SE Project - Love Letter Game](#ufaz-se-project---love-letter-game)
  - [Table Of Contents](#table-of-contents)
  - [Tools, Frameworks, Languages used](#tools-frameworks-languages-used)
    - [Frontend:](#frontend)
    - [Backend](#backend)
    - [Other tools](#other-tools)
  - [Reasoning for technical choices made](#reasoning-for-technical-choices-made)
    - [Frontend:](#frontend-1)
    - [Backend](#backend-1)
    - [Other tools](#other-tools-1)
  - [The Software interface between different modules](#the-software-interface-between-different-modules)
  - [Organization between team](#organization-between-team)
    - [Division of labour](#division-of-labour)
    - [Planning and Milestones](#planning-and-milestones)
  - [Backend Microservices](#backend-microservices)
  - [What we got out of the project](#what-we-got-out-of-the-project)
  - [Designs used in the project](#designs-used-in-the-project)
  - [Diagrams](#diagrams)
  - [Git repos](#git-repos)
  - [How run the (the not so done) project](#how-run-the-the-not-so-done-project)

## Tools, Frameworks, Languages used

### Frontend:

- Typescript
- React
- Figma
- Photoshop

### Backend

- Golang
- Gorilla web tools
- gRPC
- MySQL
- Redis
- Redis Set, List, Sorted Set Expiration Manager (Aghateymur Hasanzada's open source project)
- Microservice Architecture

### Other tools

- GitLab
- Docker, Docker-Compose

## Reasoning for technical choices made

### Frontend:

- Typescript
> We needed a solid typing system to write clean and structured code for the game core and typescript was all of that.
> We could have used pure JavaScript but this could have resulted in not so clean code and possibly could have given us
> hard times while debugging the code.

- React
>Our front-end team of this project didn't have any experience in web development and especially in front-end, that's why we decided to learn the powerful and easy to learn languages, and we first learned javascript, then we understood that to learn react js would be more powerful and we would use less same code. 


- Figma, Photoshop
> Our designers were using these tools so they weren't much of a choice.
> (Yes. We used designers who weren't from this team, who weren't even 
> from this university. We were 4 people in the group and none of us had experience
> with design before and we didn't have an extra people in our team to assign learning and
> doing the design thing. So I hope it's allowed to use designers from out of the team as
> it would be the same thing as using free assets for design)

### Backend

- Golang
> Our team wasn't good at any of backend programming languages so we had to learn 
> new language independent of which language we choose. We decided on choosing
> Golang. Apps written on Golang are lightweight as Golang is code is compiled
> to binary code. It also is designed for multithreaded apps as it uses special type
> of lightweight threads called goroutines. Also Golang's simple syntax makes it easy to
> learn it fast as we were limited with time.

- Gorilla web tools
> Although Golang has standart HTTP library out of the box, it's simple and doesn't support
> many stuff that Gorilla web tools offers. We didn't need many features but we still chose
> to use Gorilla web libraries having future demands in mind (we are thinking of working on
> the project even after the SE Course's deadline is over and improve the project). We used
> Gorilla Router and Gorilla WebSocket libraries.

- gRPC
> We used microservice architecture in backend so we needed a way to communicate between services.
> There were lots of options such as HTTP API, RPC, communication over queue softwares and gRPC.
> We chose gRPC because it is faster than some other options and also is more flexible (possible
> to use across apps written in different languages, safer error handling).

- MySQL
> We needed an SQL database to hold users data. MySQL was a simple but a powerful and one of the most
> used SQL databases. So we chose it.

- Redis
> We needed a NoSQL library with an option for expiration of the data we put in. And Redis is the perfect
> solution for that. (We even implemented session service with Redis as Golang doesn't have support for
> sessions out of the box)

- Redis Set, List, Sorted Set Expiration Manager
> This was a microservice that Aghateymur Hasanzada developed for the project but decided on
> making it open source. Although Redis supports expiration for string types, it doesn't support
> expiration for Sets, Sorted Sets and Lists but we needed that in our project.

- Microservices Architecture
> This design pattern allows easy scalability and it makes task division easier. Although it has some
> down sides such as sometimes violating DRY principles, this was not a big deal for us considering
> it's up sides.

### Other tools

- GitLab
> GitLab was chosen as remote repository, CI/CD and task management tool. The reason for this choice was because
> 1. It was free for the features we needed.
> 2. It supported 3 different tools in one app.
> 3. It doesn't limit docker image repository with size limit.
> 4. Aghateymur Hasanzada had real job experience with GitLab 

- Docker, Docker-Compose
> As we were using microservices, we needed a tool to manage all those services together using containerization.
> Docker is a perfect tool for that. Also it helped us with our local environment setup as it was OS independent.
> Frontend team didn't have to know how to run all those backend services in their local setup and backend didn't
> have to know about frontend.

## The Software interface between different modules

- Frontend <-> Backend
  - HTTP API (REST-like API)
  - WebSocket
> For in game communications we use WebSocket, for all other types of requests are handled with HTTP Interface.

- Interface between backend services
  - gRPC

## Organization between team

### Division of labour

- Aghateymur Hasanzada
> Project manager, Backend lead and developer, DevOps. Worked on Game Core on both frontend and
> backend. Set up local environment setup with docker.

- Kanan Mikayilov
> Frontend lead and manager..

- Gulzar Safarli
> Backend and AI developer.

- Sadig Gojayev

### Planning and Milestones

First milestone was set to a month after project start which was learning the technologies we were going to use.
Frontend team had to learn React and Typescript while Backend had to learn Golang, Golang interface with MySQL,
gRPC and Redis. After finishing learning process, Aghateymur Hasanzada started working on Game Core while 
Frontend team was working on App UI/UX and Gulzar Safarli was working on setting up Users Microservice.
We had in mind to make the App support Multiplayer out of the box and make it flexible for Extension Manager and AI,
so if we had enough time, we can implement them. At first we were going to implement Scrum Workflow but then decided
on Kanban Workflow as our team was small and we weren't able to work on the project full-time.

## Backend Microservices

- Users App
> Deals with User accounts, sign-in, registering and provides interface for sessions and getting unique game IDs.
> Frontend communicates over HTTP REST-like API with users app. Uses MySQL for holding user datas

- Session App
> Deals with setting sessions for different user types and also setting game IDs alongside. Provides interface for users app
> to get and set sessions. Communication is over gRPC. Uses Redis with expiration of 24h for holding sessions.

- Redis Set, Sorted Set, List manager
> Manages expiration for Sets, Sorted Sets and Lists in redis. Takes one string data in redis for each expiration request.
> Uses 4 goroutines when idle. Communication is over gRPC.

- Game App
> Deals with Game Core. Communication is over gRPC. Uses Redis to hold all the game datas.

- API App
> WebSocket interface for frontend with backend. Receives commands, parses them, processes the commands and sends related reply.
> Communication is over WebSocket.

## What we got out of the project

- Aghateymur Hasanzada
> When the teams got assembled, we talked in team and decided that I take the leadership as I have more experience. We first divided
> the team to Frontend and Backend teams. Then we decided on the technologies we were going to use in this project and started learning
> those technologies for a month. When we were working on the project the hardest thing for me was doing leadership while also working
> on code. It was putting a lot of pressure on my mind to think about the whole project architecture, divide the tasks, manage the team and
> also code at the same time because I've never been good at leadership. 
> 
> I learned lots of new technologies while working on this porject like Golang, gRPC, concurrency and so on. Also I learned teamwork, leadership,
> task management and project management.
> 
> The worst decisions I've made through the project was trying to make the code flexible so we can add extra modules iin the future. It made writing
> the code more complex so it was one of the reasons that the project wasn't done till the deadline. Currently it is really easy to add modules
> such as extension manager, AI player but none of them are implemented at the moment. However we are planning to finish the project evenn after the
> deadline is over and also make the project open source. 
> 
> The other biggest reason of not making the project till deadline was because as a team, we weren't much experienced on web development so we had to
> learn almost all of the technologies we were going to use. Although we tried our best to learn as fast as we can and then finish the project even with
> help of amateur but awesome designers, we weren't able to finish the project.
> 
> The main objective of us as a team while woorking on the project, wasn't mainly to get the best point in class but learn as much as we can from the project.
> 
> It's really unfortunate that we weren't able to finish lots of stuff that I was plannning to such as CI/CD, running the app on Google Cloud and kubernetes.

- Kanan Mikayilov
> As the leader of the team’s front-end, I’m very sorry that the game is not finished before the deadline, but we decided that we would continue to work on this game and put it open source. 
> In the last software engineering lesson, when we decided who wants to engage in front-end and who wants to engage in back-end, I immediately wanted to work on the front end, since I always had an interest in this domain, and I thought about starting to learn javascript, html, css for it. 
> It’s good that we had such an opportunity to have such experience in a large project. 
> However, we did not have any experience and knowledge in this area, we were just beginning to learn the new languages. 
> Therefore, the front-end of this project has not been completed. 
> Except of this problem, actually i learned a lot of new and very useful and powerful technologies. 
> I learned also Redux also , to make the front-end more powerful, and flexible, but i haven't had time to use this technology and optimize the front-end code.
> Plus i learned to work in a team, as it was my first team project. 
> We were having meetings regularly in 4-5 days in front-end team with Sadiq Gojayev, and planning the schedule of jobs to do for next several days. 
> I learned how to manage the project in the git properly, with creating issues to do some new updates in the project.
> We planned lots of stuff to do, and we made our best, but unfortunately we couldn't finish this project due to the lack of experience and time.

- Gulzar Safarli
> When the our team members got assembled and divided the team to Frontend and Backend teams, I wanted working on Backend, because I really like optimazing code performance and dealing with data. Also I didn't have any experience on Frontend. So we decided that I will work on Backend with Aghateymur Hasanzada and I will develope AI for our project.
>
> I learned a lot of new technologies while working on this porject like Docker, Golang, gRPC, Redis and so on. Actually learning them during one month was very hard for me. But I tried my best and I am very happy that I learned and practiced them while working on this project.
>
> Also I learned working with Git, using issues and branches properly. Most importantly, I learned to work with a team, because we implement Kanban Workflow and it was wery good beginning to be member of developers team for me.
>
> Unfortunately, we were not be able to finish our project, because we wanted to create a project that could be developed and new modules added in the future. I hope that we will continue to work on it and create great game in the future with my teammates.


- Sadig Gojayev
> My main work is all about to help frontend. Kanan and me worked with React. It was very interesting for me and first time that I joining 
> to web developing team. Our group was similar to professional. At first time It seemed to me that React and Javasciprt really hard, but 
> after Kanan helped to me to learn and try. Because I'm not interested in web developing. If conditions (covid19 and low level internet)
> was better, sure we would do great things. 

## Designs used in the project

- UI/UX Design
  - [Figma Link](https://www.figma.com/file/dYxoG4fV11bJG8QNlbgtvn/Untitled?node-id=0%3A1)
- Graphic Designs
  - [Graphic designs](https://drive.google.com/drive/folders/19rk9RG0tMPyCvamrPVj24NFohtUTJnaD?usp=sharing)

## Diagrams
- [Frontend UML Class Diagram](https://drive.google.com/file/d/1n3P4gWUjSQktqdK6zZ82M1TquNV-f6p_/view?usp=sharing)
- [Game Lifecycle Diagrams](https://gitlab.com/love-letter-ufaz-se/love-letter-game-frontend/-/wikis/Game-Lifecycle)

## Git repos
- [Love Letter Game Docker](https://gitlab.com/love-letter-ufaz-se/love-letter-game-docker)
  - Contains docker-compose.yml, local envireonment setup script and report
- [Love Letter Game Users App](https://gitlab.com/love-letter-ufaz-se/love-letter-game-users-app)
  - Contains Users microservice
- [Love Letter Game API App](https://gitlab.com/love-letter-ufaz-se/love-letter-game-api-app)
  - Contains API microservice
- [Love Letter Game Game App](https://gitlab.com/love-letter-ufaz-se/love-letter-game-game-app)
  - Contains Game Core microservice
- [Love Letter Game Session App](https://gitlab.com/love-letter-ufaz-se/love-letter-game-session-app)
  - Contains Session microservice
- [Love Letter Game Frontend](https://gitlab.com/love-letter-ufaz-se/love-letter-game-frontend)
  - Contains Frontend App
- [Redis Set, Sorted Set, List Expiration Manager](https://gitlab.com/ShapedHorizon/redis-set-list-expiration-manager)
  - Open Source project that's used as a microservice.

## How run the (the not so done) project
First ensure that you have docker and docker-compose installed in your system.
Then clone the [Love Letter Game Docker](https://gitlab.com/love-letter-ufaz-se/love-letter-game-docker) repo.
At that repor, start `localsetup.sh` script. This will clone all other needed repos.
Then you can run the project with `docker-compose up`. The website will be up on port `8080`.
